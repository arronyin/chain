package com.arron.blog.controller;


import cn.hutool.core.util.StrUtil;
import com.arron.utils.ThreadUtils;

/**
 * @Auther: arronbin
 * @Date: 2019/7/12 20:54
 * @Description: 测试
 */
public class TestController {

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        ThreadUtils.execute(()->{
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        ThreadUtils.shutdown(1000);
        long endTime = System.currentTimeMillis();
        System.out.println(StrUtil.format("任务耗时{}",(endTime-startTime)));
    }
}
