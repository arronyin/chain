package com.arron.core.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Auther: wenbin.yin
 * @Date: 2019/7/12 13:42
 * @Description:
 */
@Data
public class BaseEntity implements Serializable{
    private static final long serialVersionUID = 1L;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新者
     */
    private String updateBy;

    /**
     * 更新时间
     */
    private Date updateTime;

}
