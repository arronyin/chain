package com.arron.utils;

/**
 * @Auther: arronbin
 * @Date: 2019/7/13 11:25
 * @Description: 给定时间让线程执行任务，超时自动结束
 */
public class ThreadUtils {
    private static Thread executeThread;

    private static boolean finished = false;

    public static void execute(Runnable task){
        executeThread = new Thread(()->{
             Thread executeThreadDaemon = new Thread(task);
             executeThreadDaemon.setDaemon(true);
             executeThreadDaemon.start();
            try {
                executeThreadDaemon.join();
                finished = true;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        executeThread.start();
    }

    public static void shutdown(long mills){
        long currentTime = System.currentTimeMillis();
        while (!finished){
            if(System.currentTimeMillis()-currentTime>=mills){
                System.out.println("任务超时...");
                executeThread.interrupt();
                break;
            }
            try {
                executeThread.sleep(10);
            } catch (InterruptedException e) {
                System.out.println("执行线程任务被打断...");
                e.printStackTrace();
            }
        }
    }
}
