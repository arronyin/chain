package com.arron.designPattern;

import cn.hutool.core.util.StrUtil;
import com.arron.designPattern.decorator.Drink;
import com.arron.designPattern.decorator.LongBlack;
import com.arron.designPattern.decorator.Milk;

/**
 * @Auther: arronbin
 * @Date: 2019/11/30 09:55
 * @Description:
 */
public class Test {
    public static void main(String[] args) {
        Drink order = new LongBlack();
        System.out.println(StrUtil.format("费用={}，描述={}",order.cost(),order.getDes()));
        order = new Milk(order);
        System.out.println(StrUtil.format("加入一份牛奶后费用={}，描述={}",order.cost(),order.getDes()));
    }
}
