package com.arron.designPattern.decorator;

import java.math.BigDecimal;

/**
 * @Auther: arronbin
 * @Date: 2019/11/30 22:19
 * @Description:
 */
public class Coffee extends Drink {
    @Override
    public BigDecimal cost() {
        return super.getPrice();
    }
}
