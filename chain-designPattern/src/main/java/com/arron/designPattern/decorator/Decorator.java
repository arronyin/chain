package com.arron.designPattern.decorator;

import cn.hutool.core.util.StrUtil;

import java.math.BigDecimal;

/**
 * @Auther: arronbin
 * @Date: 2019/11/30 22:23
 * @Description:
 */
public class Decorator extends Drink {

    private Drink obj;

    public Decorator(Drink obj) {
        this.obj = obj;
    }

    @Override
    public BigDecimal cost() {
        return super.getPrice().add(obj.getPrice());
    }

    @Override
    public String getDes() {
        return StrUtil.format("{} {} {} ",super.getDes(),super.getPrice(),obj.getDes());
    }
}
