package com.arron.designPattern.decorator;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Auther: arronbin
 * @Date: 2019/11/30 22:18
 * @Description:
 */
@Data
public abstract class Drink {

    private String des;

    private BigDecimal price = BigDecimal.ONE;

    public  abstract BigDecimal cost();

}
