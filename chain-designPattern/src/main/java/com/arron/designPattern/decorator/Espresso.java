package com.arron.designPattern.decorator;

import java.math.BigDecimal;

/**
 * @Auther: arronbin
 * @Date: 2019/11/30 22:21
 * @Description:
 */
public class Espresso extends Coffee {

    public Espresso() {
        setDes("意大利咖啡");
        setPrice(BigDecimal.valueOf(6));
    }
}
