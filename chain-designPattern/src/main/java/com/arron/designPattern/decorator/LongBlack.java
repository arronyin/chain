package com.arron.designPattern.decorator;

import java.math.BigDecimal;

/**
 * @Auther: arronbin
 * @Date: 2019/11/30 22:21
 * @Description:
 */
public class LongBlack extends Coffee {

    public LongBlack() {
        setDes("LongBlack咖啡");
        setPrice(BigDecimal.valueOf(5));
    }
}
