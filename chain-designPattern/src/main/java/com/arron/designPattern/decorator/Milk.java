package com.arron.designPattern.decorator;

import java.math.BigDecimal;

/**
 * @Auther: arronbin
 * @Date: 2019/11/30 22:32
 * @Description:
 */
public class Milk extends Decorator {

   public Milk(Drink obj){
        super(obj);
        setDes("巧克力");
        setPrice(BigDecimal.valueOf(2.1));
   }

}
