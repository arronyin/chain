package com.arron.blog.domain;

import com.arron.core.domain.BaseEntity;
import lombok.Data;

/**
 * @Auther: wenbin.yin
 * @Date: 2019/7/12 13:51
 * @Description:
 */
@Data
public class Blog extends BaseEntity {

    private static final long serialVersionUID = 8645949034797742847L;

    /**
     * 博客id
     */
    private Integer blogId;

    /**
     * 归类id
     */
    private Integer categoryId;

    /**
     * 博文标题
     */
    private String title;

    /**
     * 博文摘要
     */
    private String summary;

    /**
     * 博文状态，1表示已经发表，2表示在草稿箱，3表示在垃圾箱
     */
    private String status;

    /**
     * 权重
     */
    private Integer weight;

    /**
     * 是否推荐，Y表示推荐，N表示不推荐
     */
    private String support;

    /**
     * 点击次数
     */
    private Integer click;

    /**
     * 标图展示地址
     */
    private String headerImg;

    /**
     * 博文类型，1表示markdown，2表示富文本
     */
    private String type;

    /**
     * 博文正文内容
     */
    private String content;

}
