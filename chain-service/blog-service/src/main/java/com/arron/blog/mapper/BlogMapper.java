package com.arron.blog.mapper;

import com.arron.blog.domain.Blog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Auther: wenbin.yin
 * @Date: 2019/7/12 13:57
 * @Description:
 */
public interface BlogMapper extends BaseMapper<Blog> {
}
