package com.arron.blog.service;


import com.arron.blog.domain.Blog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Auther: wenbin.yin
 * @Date: 2019/7/12 13:58
 * @Description:
 */
public interface BlogService extends IService<Blog> {
}
