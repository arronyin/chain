package com.arron.blog.service.impl;

import com.arron.blog.domain.Blog;
import com.arron.blog.mapper.BlogMapper;
import com.arron.blog.service.BlogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @Auther: wenbin.yin
 * @Date: 2019/7/12 14:02
 * @Description:
 */
@Service
public class BlogServiceImpl extends ServiceImpl<BlogMapper, Blog> implements BlogService {
}
