package com.arron.blog.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.Environment;

import java.util.Arrays;

/**
 * @Auther: arronbin
 * @Date: 2019/8/22 21:51
 * @Description:
 */
public class SpringTest {

    public static void main(String[] args) {
        ApplicationContext applicationContext =  new AnnotationConfigApplicationContext();
        String[]arrs = applicationContext.getBeanDefinitionNames();
        Arrays.stream(arrs).forEach(System.out::println);
        Environment environment = applicationContext.getEnvironment();
        System.out.println(environment.getProperty("os.name"));
    }

}
