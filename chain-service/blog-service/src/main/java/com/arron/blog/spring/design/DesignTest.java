package com.arron.blog.spring.design;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Auther: arronbin
 * @Date: 2019/8/22 21:51
 * @Description:
 */
public class DesignTest implements Runnable{

    static final int THREAD_COUNT = 1000;
    static final CountDownLatch latch = new CountDownLatch(THREAD_COUNT);
    static final DesignTest demo = new DesignTest();

    public static void main(String[] args) throws InterruptedException {
        ExecutorService exec = Executors.newFixedThreadPool(THREAD_COUNT);
        for (int i=0; i<THREAD_COUNT; i++){
            exec.submit(demo);
        }

        // 等待检查
        latch.await();

        // 发射火箭
        System.out.println("Fire!");
        // 关闭线程池
        exec.shutdown();
    }

    @Override
    public void run() {
        // 模拟检查任务
        try {
//            System.out.println(HungryType.getInstance()); // 饿汉式天生线程安全
//            System.out.println(LanHanShi.getInstance());// 第一种懒汉式（非安全）
//            System.out.println(LanHanShiOne.getInstance());// 第二种懒汉式（加 synchronized 锁，性能差）
            System.out.println(LanHanShiTwo.getInstance());// 第二种懒汉式（加 synchronized 锁）
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //计数减一
            //放在finally避免任务执行过程出现异常，导致countDown()不能被执行
            latch.countDown();
        }
    }
}
