package com.arron.blog.spring.design;

import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @Auther: arronbin
 * @Date: 2019/8/22 21:55
 * @Description: 饿汉式
 */
public class HungryType {

    private final static HungryType instance = new HungryType();

    public HungryType() {}

    public static HungryType getInstance(){
        return instance;
    }

}
