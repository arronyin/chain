package com.arron.blog.spring.design;

/**
 * @Auther: arronbin
 * @Date: 2019/8/22 22:09
 * @Description: 懒汉式
 */
public class LanHanShi {

    private static LanHanShi instance = null;

    public LanHanShi() {}

    public static LanHanShi getInstance(){
        if(instance == null){
            instance = new LanHanShi();
        }
        return instance;
    }
}
