package com.arron.blog.spring.design;

/**
 * @Auther: arronbin
 * @Date: 2019/8/22 22:09
 * @Description: 懒汉式变种1
 */
public class LanHanShiOne {

    private static LanHanShiOne instance = null;

    public LanHanShiOne() {}

    public synchronized static LanHanShiOne getInstance(){
        if(instance == null){
            instance = new LanHanShiOne();
        }
        return instance;
    }
}
