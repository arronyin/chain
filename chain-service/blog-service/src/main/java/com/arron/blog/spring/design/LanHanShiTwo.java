package com.arron.blog.spring.design;

/**
 * @Auther: arronbin
 * @Date: 2019/8/22 22:09
 * @Description: 懒汉式变种1
 */
public class LanHanShiTwo {

    private static volatile LanHanShiTwo instance = null;

    public LanHanShiTwo() {}

    public  static LanHanShiTwo getInstance(){
        if(instance == null){
            synchronized(LanHanShiTwo.class){
               if(instance == null){
                   instance = new LanHanShiTwo();
               }
            }
        }
        return instance;
    }
}
