package com.arron.entity;

import lombok.Data;

/**
 * @Auther: wenbin.yin
 * @Date: 2019/9/1 14:43
 * @Description:
 */
@Data
public class User {

    private Long userId;

    private String userName;

    private String password;

    private String email;

    private String header;

    private String secretKey;
}
