package com.arron.ibatis;

/**
 * @Auther: arronbin
 * @Date: 2019/9/3 21:55
 * @Description:
 */
public class IBatisTest {

    public static void main(String[] args) {
        ISqlSession sqlSession = new ISqlSession(new IConfiguration(),new SimpleExecutor());
        TestMapper testMapper = sqlSession.getMapper(TestMapper.class);
        Test test = testMapper.queryTestById("180425B0B7N6B25P");
        System.out.println(test);
    }

}
