package com.arron.ibatis;

import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

/**
 * @Auther: arronbin
 * @Date: 2019/9/3 21:37
 * @Description:
 */
public class IConfiguration {

    <T> T getMapper(Class<T> clazz,ISqlSession sqlSession){
        return (T) Proxy.newProxyInstance(this.getClass().getClassLoader(),new Class[]{clazz},new IMapperProxy(sqlSession));
    }

    static class TestMapperXml{
        public static final String namespace = "com.arron.ibatis.TestMapper";

        public static final Map<String,String> methodSqlMapping = new HashMap<>();

        static {
            methodSqlMapping.put("queryTestById","select * from users where id = ?");
        }
    }

}
