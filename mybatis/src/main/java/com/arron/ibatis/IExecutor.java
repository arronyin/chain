package com.arron.ibatis;

/**
 * @Auther: arronbin
 * @Date: 2019/9/3 21:37
 * @Description:
 */
public interface IExecutor {
    <T> T query(String statment, String parm);
}
