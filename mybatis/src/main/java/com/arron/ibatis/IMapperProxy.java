package com.arron.ibatis;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @Auther: arronbin
 * @Date: 2019/9/3 21:41
 * @Description:
 */
public class IMapperProxy implements InvocationHandler {

    private ISqlSession sqlSession;

    public IMapperProxy(ISqlSession sqlSession) {
        this.sqlSession = sqlSession;
    }

    @Override
    public Object invoke(Object obj, Method method, Object[] objects) throws Throwable {
        if(method.getDeclaringClass().getName().equals(IConfiguration.TestMapperXml.namespace)){
            String sql = IConfiguration.TestMapperXml.methodSqlMapping.get(method.getName());
            return sqlSession.selectOne(sql,String.valueOf(objects[0]));
        }
        return method.invoke(obj,objects);
    }

}
