package com.arron.ibatis;

/**
 * @Auther: arronbin
 * @Date: 2019/9/3 21:36
 * @Description:
 */
public class ISqlSession {

    private IConfiguration configuration;

    private IExecutor executor;

    public ISqlSession(IConfiguration configuration, IExecutor executor) {
        this.configuration = configuration;
        this.executor = executor;
    }

    public <T>T getMapper(Class<T>clazz){
        return configuration.getMapper(clazz,this);
    }

    public <T>T selectOne(String statment,String parm){
        return executor.query(statment,parm);
    }
}
