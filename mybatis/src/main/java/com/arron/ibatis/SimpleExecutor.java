package com.arron.ibatis;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * @Auther: arronbin
 * @Date: 2019/9/3 21:56
 * @Description:
 */
public class SimpleExecutor implements IExecutor{
    @Override
    public <T> T query(String statment, String parm) {
       try {
           Test test = new Test();
           Class.forName("com.mysql.jdbc.Driver");
           String url = "jdbc:mysql://localhost:3306/egg_video?characterEncoding=utf-8&useSSL=false&serverTimezone=UTC";
           String user = "root";
           String pwd = "m1234";

           Connection conn = DriverManager.getConnection(url, user, pwd);
           PreparedStatement ps = conn.prepareStatement(statment);
           ps.setString(1, parm);

           ResultSet rs = ps.executeQuery();
           while(rs.next()){
               test.setId(rs.getString("id"));
               test.setUsername(rs.getString("username"));
               test.setNickname(rs.getString("nickname"));
           }
           rs.close();
           ps.close();
           conn.close();
           return (T)test;
       }catch (Exception e){
           e.printStackTrace();
       }
       return null;
    }
}
