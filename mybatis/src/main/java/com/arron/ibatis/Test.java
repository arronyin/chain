package com.arron.ibatis;

import lombok.Data;

/**
 * @Auther: arronbin
 * @Date: 2019/9/3 21:30
 * @Description:
 */
@Data
public class Test {

    private String id;

    private String username;

    private String nickname;

}
