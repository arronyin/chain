package com.arron.ibatis;

/**
 * @Auther: arronbin
 * @Date: 2019/9/3 21:49
 * @Description:
 */
public interface TestMapper {

    Test queryTestById(String id);

}
