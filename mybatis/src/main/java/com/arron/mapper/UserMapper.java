package com.arron.mapper;

import com.arron.entity.User;

/**
 * @Auther: wenbin.yin
 * @Date: 2019/9/1 14:52
 * @Description:
 */
public interface UserMapper {
     User findUserById(int id) throws Exception;
}
